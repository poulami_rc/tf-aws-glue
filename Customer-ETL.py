import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

args = getResolvedOptions(sys.argv, ['JOB_NAME'])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

# Script generated for node Amazon S3
AmazonS3_node1710679167971 = glueContext.create_dynamic_frame.from_catalog(database="tf_customer_database", table_name="customer_csv", transformation_ctx="AmazonS3_node1710679167971")

# Script generated for node Change Schema
ChangeSchema_node1710680072752 = ApplyMapping.apply(frame=AmazonS3_node1710679167971, mappings=[("customerid", "long", "customer_id", "long"), ("namestyle", "boolean", "namestyle", "boolean"), ("title", "string", "title", "string"), ("firstname", "string", "first_name", "string"), ("middlename", "string", "middle_name", "string"), ("lastname", "string", "last_name", "string"), ("suffix", "string", "suffix", "string"), ("company_name", "string", "company_name", "string"), ("salesperson", "string", "sales_person", "string"), ("emailaddress", "string", "email_id", "string"), ("phone", "string", "phone", "string"), ("passwordhash", "string", "passwordhash", "string"), ("passwordsalt", "string", "passwordsalt", "string"), ("rowguid", "string", "rowguid", "string"), ("modifieddate", "string", "modified_date", "string")], transformation_ctx="ChangeSchema_node1710680072752")

# Script generated for node Amazon S3
AmazonS3_node1710680300314 = glueContext.write_dynamic_frame.from_options(frame=ChangeSchema_node1710680072752, connection_type="s3", format="glueparquet", connection_options={"path": "s3://poulami-glue-practice/target/", "partitionKeys": []}, format_options={"compression": "uncompressed"}, transformation_ctx="AmazonS3_node1710680300314")

job.commit()