variable "bucket_name" {
  type    = string
  default = "poulami-glue-practice"
  description = "S3 bucket name"
}

variable "data_location" {
  type    = string
  default = "data/customer_database/customer_csv/"
  description = "data file location inside S3 bucket"
}

variable "data_file_type" {
  type    = string
  default = ".csv"
  description = "data file type/extension"
}

variable "etl_job_schedule" {
  type    = string
  default = "cron(15 12 * * ? *)"
  description = "etl job schedule (in cron format)"
}
