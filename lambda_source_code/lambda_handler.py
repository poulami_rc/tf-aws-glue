import boto3

glue = boto3.client('glue')

def handler(event, context):
    gluejobname="tf_customer_etl_job"

    try:
        runId = glue.start_job_run(JobName=gluejobname)
        status = glue.get_job_run(JobName=gluejobname, RunId=runId['JobRunId'])
        print("Job Status : ", status['JobRun']['JobRunState'])
    except Exception as e:
        print(e)        
        raise e