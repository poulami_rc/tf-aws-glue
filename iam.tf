#--------------------access policy for lambda function----------------------

#use AWS-managed AWSLambdaBasicExecutionRole 
data "aws_iam_policy" "lambda_basic_execution_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

#use AWS-managed AWSGlueServiceRole 
data "aws_iam_policy" "lambda_glue_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"
}

#lambda iam role
resource "aws_iam_role" "iam_role_for_lambda" {
  name = "tf_iam_for_lambda_glue_trigger"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

#attach AWS-managed AWSLambdaBasicExecutionRole policy to Lambda IAM role
resource "aws_iam_role_policy_attachment" "basic_execution_policy_attach" {
  role       = aws_iam_role.iam_role_for_lambda.id
  policy_arn = data.aws_iam_policy.lambda_basic_execution_policy.arn
}

#attach AWS-managed AWSGlueServiceRole policy to Lambda IAM role
resource "aws_iam_role_policy_attachment" "glue_policy_attach" {
  role       = aws_iam_role.iam_role_for_lambda.id
  policy_arn = data.aws_iam_policy.lambda_glue_policy.arn
}

#--------------------access policy for glue----------------------

#use AWS-managed AWSGlueConsoleFullAccess 
data "aws_iam_policy" "glue_full_access" {
  arn = "arn:aws:iam::aws:policy/AWSGlueConsoleFullAccess"
}

#policy document to grant read-write access to s3 bucket
data "aws_iam_policy_document" "s3bucket_access" {
  statement {
    sid = "1"

    actions = [
      "s3:*"
    ]

    resources = [      
      "${data.aws_s3_bucket.s3_bucket.arn}/*"
    ]
  } 
}

#custom policy with the policy document
resource "aws_iam_policy" "s3bucket_access_policy" {
  name   = "tf_glue_s3bucket_access_policy"
  path   = "/"
  policy = data.aws_iam_policy_document.s3bucket_access.json
}

#glue iam role
resource "aws_iam_role" "iam_role_for_glue" {
  name = "tf_iam_role_for_glue"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "glue.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

#attach AWS-managed AWSGlueConsoleFullAccess policy to Glue IAM role
resource "aws_iam_role_policy_attachment" "glue_full_access_attach" {
  role       = aws_iam_role.iam_role_for_glue.id
  policy_arn = data.aws_iam_policy.glue_full_access.arn
}

#attach custom s3 bucket policy to Glue IAM role
resource "aws_iam_role_policy_attachment" "s3_full_access_attach" {
  role       = aws_iam_role.iam_role_for_glue.id
  policy_arn = aws_iam_policy.s3bucket_access_policy.arn
}
