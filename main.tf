terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "http" {
  }
}

provider "aws" {
  region = "ap-south-1"
}

#s3 bucket
data "aws_s3_bucket" "s3_bucket" {
  bucket = var.bucket_name
}

resource "aws_s3_object" "object" {
  bucket = data.aws_s3_bucket.s3_bucket.bucket 
  key    = "scripts/Customer-ETL.py"
  source = "./Customer-ETL.py"
}
