#Lambda function code
data "archive_file" "lambda_zip" {
    type          = "zip"
    source_dir    = "${path.module}/lambda_source_code"
    output_path   = "${path.module}/lambda_function.zip"
    depends_on = [random_string.r]
}

resource "random_string" "r" {
  length  = 16
  special = false
}

#Lambda function configuration
resource "aws_lambda_function" "lambda" {
  filename         = data.archive_file.lambda_zip.output_path
  function_name    = "tf_lambda_glue_trigger"
  role             = "${aws_iam_role.iam_role_for_lambda.arn}"
  handler          = "lambda_handler.handler"
  source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"
  runtime          = "python3.10"
  timeout          = 10
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = data.aws_s3_bucket.s3_bucket.arn
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = data.aws_s3_bucket.s3_bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = var.data_location
    filter_suffix       = var.data_file_type
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}