resource "aws_glue_catalog_database" "database" {
  name = "tf_customer_database"
  location_uri = "s3://${data.aws_s3_bucket.s3_bucket.bucket}/${var.data_location}"
}

resource "aws_glue_crawler" "crawler" {
  database_name = aws_glue_catalog_database.database.name
  name          = "tf_customer_csv_crawler"
  role          = aws_iam_role.iam_role_for_glue.arn
  
  s3_target {
    path = "s3://${data.aws_s3_bucket.s3_bucket.bucket}/${var.data_location}"
  }
}

resource "aws_glue_job" "job" {
  name     = "tf_customer_etl_job"
  role_arn = aws_iam_role.iam_role_for_glue.arn

  command {
    script_location = "s3://${data.aws_s3_bucket.s3_bucket.bucket}/scripts/Customer-ETL.py"
    python_version  = "3"  
  }

  default_arguments = {    
    "--job-language"    = "python"
    "--enable-spark-ui" = "true"
    "--spark-event-logs-path" = "s3://${data.aws_s3_bucket.s3_bucket.bucket}/tmp/"    
    "--TempDir" = "s3://${data.aws_s3_bucket.s3_bucket.bucket}/tmp/"
  }
}

resource "aws_glue_trigger" "scheduled_trigger" {
  name     = "tf_glue_job_scheduled_trigger"
  schedule = var.etl_job_schedule
  type     = "SCHEDULED"

  actions {
    job_name = aws_glue_job.job.name
  }
}
